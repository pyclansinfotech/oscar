########## import modules ########################
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from models import Products, Register
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import check_password
from django.shortcuts import render_to_response, redirect
from django.template.loader import render_to_string

@csrf_exempt
def index(request):
	if (request.method=='POST'):
		product_list=[]
		product_dict={'id':'','name':'','price':'','image':''}
		pro_id=request.POST['id']
		image=request.POST['image']
		name=request.POST['name']
		price=request.POST['price']
		total=request.POST['total']
		print 'here is total>>>>', total
		product_dict['id']=pro_id
		product_dict['name']=name
		product_dict['price']=price
		product_dict['image']=image
		product_list.append(product_dict.copy())
		html = render_to_string('cart.html', {'product':product_list,'total':total})
		return HttpResponse(html)
		#return render(request, 'home_v1.html',{'product':product_list})
	else:
		product_list=[]
		product_dict={'name':'','desc':'','price':'','image':'','brand':''}
		products=Products.objects.all()
		for product in products:
			product_id=product.id
			product_name=product.name_pro
			product_desc=product.description
			product_price=product.price
			product_image=product.image_pro
			product_brand=product.brand
			product_dict['id']=product_id
			product_dict['name']=product_name
			product_dict['desc']=product_desc
			product_dict['price']=product_price
			product_dict['image']=product_image
			product_dict['brand']=product_brand
			product_list.append(product_dict.copy())
		return render(request, 'home_v1.html', {'products':product_list})

#############Login###################
@csrf_exempt
def login(request):
	try:
		if request.method == 'POST':
			username=request.POST['username']
			password=request.POST['password']
			user_data=Register.objects.filter(username=username)
			if not user_data:
				return HttpResponse('non-authentic')
			else:
				for us in user_data:
					user_password=us.password
					#check=check_password(password,user_password)
				if (user_password==password):
					request.session['loggedin_user']=username
					return HttpResponse('authentic')
				else:
					return HttpResponse('non-authentic')
		else:
			return False
	except Exception as e:
		print 'error',e

#############Render Dashboard###################
@csrf_exempt
def dashboard(request):
	try:
		logged_in_user=request.session['loggedin_user']
		return render(request,'dashboard.html',{'logged_in_user':logged_in_user})
		
	except Exception as e:
		print 'error',e

############Logout###################
@csrf_exempt
def logout(request):
	try:
		request.session['loggedin_user']=''
		return render(request,'home_v1.html')
		
	except Exception as e:
		print 'error',e


def my_account(request):
	return render(request, 'create_an_account.html')

def list_order(request):
	return render(request, 'orders_list.html')

def checkout(request):
	return render(request, 'order_info.html')

def about_us(request):
	return render(request, 'text_page.html')

def contacts(request):
	return render(request, 'contact.html')

def camera(request):
	return render(request, 'category_v1.html')

def blog(request):
	return render(request, 'blog.html')

def computers(request):
	return render(request, 'category_v2.html')


