"""djoscar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'djoscar.views.index', name='index'),
    url(r'^my_account/$', 'djoscar.views.my_account', name='my_account'),
    url(r'^list_order/$', 'djoscar.views.list_order', name='list_order'),
    url(r'^checkout/$', 'djoscar.views.checkout', name='checkout'),
    url(r'^about_us/$', 'djoscar.views.about_us', name='about_us'),
    url(r'^contacts/$', 'djoscar.views.contacts', name='contacts'),
    url(r'^camera/$', 'djoscar.views.camera', name='camera'),
    url(r'^computers/$', 'djoscar.views.computers', name='computers'),
    url(r'^blog/$', 'djoscar.views.blog', name='blog'),
    url(r'^login/$', 'djoscar.views.login', name='login'),
    url(r'^dashboard/$', 'djoscar.views.dashboard', name='dashboard'),
    url(r'^logout/$', 'djoscar.views.logout', name='logout'),

]
